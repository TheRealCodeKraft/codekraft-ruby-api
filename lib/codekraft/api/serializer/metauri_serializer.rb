module Codekraft
  module Api
    module Serializer
      class MetauriSerializer < Base

        attributes :uri, :title, :description, :image, :metas

        def title
					if not object.metas.nil?
	          JSON.parse(object.metas)["best_title"]
					end
        end

        def description
					if not object.metas.nil?
	          JSON.parse(object.metas)["best_description"]
					end
        end

        def image
					if not object.metas.nil?
	          metas = JSON.parse(object.metas)
  	        metas["images"].size > 0 ? metas["images"][0] : nil
					end
        end

      end
    end
  end
end
