module Codekraft
  module Api
    module Mailer
      class UserMailer < Base
        def invite user_id
          @user = Codekraft::Api::Model::User.find(user_id)

          stamp = SecureRandom.uuid
          @user.stamp_salt = BCrypt::Engine.generate_salt
          @user.stamp = Codekraft::Api::Service::User.new.encrypt_password(stamp, @user.stamp_salt)
					@user.stamp_expiration = 1.week.from_now
          @user.save!

					@type = "user-invite"
          @url = base_url + "/dashboard?email=#{@user.email}&stamp=#{stamp}"
					@title = ENV["INVITATION_MAIL_TITLE"] || "Vous êtes invité à rejoindre la plateforme"
					@site_url = base_url

					@header_title = ENV["INVITATION_MAIL_HEADER_TITLE"] || @title
          mail(to: @user.email, from:Codekraft::Api.configuration.default_mail_from, subject: @title)

        end

        def confirm user_id
          @user = Codekraft::Api::Model::User.find(user_id)

					@type = "user-confirm"
					@title = ENV["USER_CREATION_MAIL_TITLE"] || "Bienvenue, vous avez créé votre compte sur la plateforme"
					@header_title = ENV["USER_CREATION_MAIL_HEADER_TITLE"] || @title
					@site_url = base_url

          mail(to: to(@user), from: from, subject: @title)

        end

				def blocked user_id
					@user = Codekraft::Api::Model::User.find(user_id)

					stamp = SecureRandom.uuid
          @user.stamp_salt = BCrypt::Engine.generate_salt
          @user.stamp = Codekraft::Api::Service::User.new.encrypt_password(stamp, @user.stamp_salt)
          @user.stamp_expiration = 2.days.from_now
          @user.save!

					@type = "user-blocked"
          @url = base_url + "/reset-password?email=#{@user.email}&key=#{stamp}"
					@title = ENV["USER_BLOCKED_MAIL_TITLE"] || "Votre compte a été bloqué"
					@header_title = ENV["USER_BLOCKED_MAIL_HEADER_TITLE"] || @title
					@site_url = base_url

          mail(to: to(@user), from: from, subject: @title)

				end
      end
    end
  end
end
