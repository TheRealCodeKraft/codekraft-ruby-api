module Codekraft
  module Api
    module Mailer
      class ForgotPasswordMailer < Base
        def reset_password user_id
          user = Codekraft::Api::Model::User.find(user_id)

          stamp = SecureRandom.uuid
          user.stamp_salt = BCrypt::Engine.generate_salt
          user.stamp = Codekraft::Api::Service::User.new.encrypt_password(stamp, user.stamp_salt)
          user.stamp_expiration = 2.days.from_now
          user.save!

          @user = user
					endpoint = ENV["ENDPOINT_RESET_PASSWORD"] || "/reset-password"
					@url = base_url + endpoint + "?email=#{user.email}&key=#{stamp}"
          @site_url = base_url

					@type = "forgot-password"
					@title = ENV["RESET_PASSWORD_MAIL_TITLE"] || "Changer votre mot de passe"
					@header_title = ENV["RESET_PASSWORD_MAIL_HEADER_TITLE"] || @title

					mail(to: to(user), from: from, subject: @title)
        end
      end
    end
  end
end
