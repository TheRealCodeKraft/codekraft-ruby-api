module Codekraft
	module Api
		module Mailer
			class NotificationMailer < Base

				def push notification
					@url = base_url
					@user = notification.recipient
					@notification = notification
          mail(	to: notification.recipient.email,
								from: from,
								subject: notification.description
					)
				end

			end
		end
	end
end
