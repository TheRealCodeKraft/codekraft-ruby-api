module Codekraft
  module Api
    module Mailer
      class Base < ActionMailer::Base
        default from: Proc.new { default_from }
        layout 'mailer'

        def base_url
          Codekraft::Api.configuration.front_url
        end

				def to user
					"#{user.firstname} #{user.lastname} <#{user.email}>"
				end

				def from
					default_from
				end

				private
					def default_from
						from = Codekraft::Api.configuration.default_mail_from
						if defined? Codekraft::Api.configuration.default_mail_from_name
								from = "#{Codekraft::Api.configuration.default_mail_from_name} <#{Codekraft::Api.configuration.default_mail_from}>"
						end

						from
					end
      end
    end
  end
end
