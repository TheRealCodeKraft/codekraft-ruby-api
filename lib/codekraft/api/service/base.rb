module Codekraft
  module Api
    module Service
      class Base
        def initialize(model)
          @model = model
        end

        def model
          @model
        end

        def setCurrentUser user
          @current_user = user
          return self
        end

        def fetchAll params
          if params.has_key? :all
            params.delete :all
          end

          params = self.community_filter(params)
        end

        def fetchOne params
          @model.find(params[:id])
        end

        def fetchOneBy params
          @model.find_by(params)
        end

        def create params
          params = self.communities_manager params
          @model.create!(params)
        end

        def update params
          entity = @model.find(params[:id])
          params.delete :id
          params = self.communities_manager params
          entity.update!(params)
          entity
        end

        def destroy params
          @model.destroy(params[:id])
        end

        def upload params
          entity = @model.find(params[:id])

          paramClassName = ""
          if @model.reflect_on_all_associations.select { |assoc| assoc.name == params[:fieldname].to_sym }.size > 0
            paramClassName = @model.reflect_on_all_associations.select { |assoc| assoc.name == params[:fieldname].to_sym }[0].class_name
          end

          if paramClassName == "Codekraft::Api::Model::Attachment"
            params[params[:fieldname]] = Codekraft::Api::Model::Attachment.create!({attachment: ActionDispatch::Http::UploadedFile.new(params[params[:fieldname]])})
          else
            params[params[:fieldname]] = ActionDispatch::Http::UploadedFile.new(params[params[:fieldname]])
          end

          params.delete :fieldname
          entity.update!(params)
          @model.find(entity.id)
        end

        def deleteFile params
          entity = @model.find(params[:id])
          entity.public_send(params[:fieldname]).destroy
          #entity.sheet.destroy
          entity.save!
          @model.find(entity.id)
        end

        def is_admin?
          if @current_user and @current_user.role == "admin"
            return true
          end

          return false
        end

        def order query, sorter, params
          query.reorder("#{sorter[:target]} #{sorter[:direction]}")
        end

        def communities_manager params
          if params.has_key? :community_id
            params[:community] = Community.find(params[:community_id])
            params.delete :community_id
          elsif not @current_user.nil? and @current_user.respond_to? :communities and @current_user.communities.size == 1
            assocs = @model.reflect_on_all_associations(:belongs_to).map(&:name)
            if assocs.include?(:community)
              params[:community] = @current_user.communities.first
            end
          end
          if params.has_key? :communities
            params[:communities] = params[:communities].map{ |id| Community.find(id) }
          end
          params
        end

        def community_filter params
          query = @model

          if @current_user.nil?
            return query.where(params)
          end

          communities = self.is_admin?() ? @current_user.admin_communities : @current_user.communities
          communityIds = communities.map(&:id);

          if params.has_key? :community_id and not communityIds.include?(params[:community_id].to_i)
            params[:community_id] = -1;
            return query.where(params)
          end

          assocOne = @model.reflect_on_all_associations(:belongs_to).map(&:name)
          assocMany = @model.reflect_on_all_associations(:has_and_belongs_to_many).map(&:name)

          if not assocOne.include?(:community) and not assocMany.include?(:communities)
            if params.has_key? :community_id
              params.delete :community_id
            end
            return query.where(params)
          end

          if assocOne.include?(:community) and not params.has_key? :community_id
            params[:community_id] = communities.map(&:id)
          end

          if assocMany.include?(:communities)
            if not params.has_key? :community_id
              query = query.joins(:communities).where(communities: {id: communities.map(&:id)})
            else
              query = query.joins(:communities).where(communities: {id: params[:community_id] })
              params.delete :community_id
            end
          end

          query.where(params)
        end

      end
    end
  end
end
