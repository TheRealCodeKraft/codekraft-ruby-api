module Codekraft
  module Api
    module Service
      class Post < Base

        def initialize model
          super(model)
        end

        def update params
          if params.has_key? :attachments
            attachments = []
            if not params[:attachments].empty?
              params[:attachments].each do |attachment|
								if not attachment.empty?
									a = JSON.parse("{}")
									if attachment.is_a? String
										a = JSON.parse(attachment)
									end
									if a.has_key? "id"
										attachment = Codekraft::Api::Model::Attachment.find(a["id"])
									else
	puts attachment.inspect
										attachment = Codekraft::Api::Model::Attachment.create!({attachment: ActionDispatch::Http::UploadedFile.new(attachment), user: @current_user})
									end
									attachments << attachment
								end
              end
            end
            params[:attachments] = attachments
          end
          super(params)
        end

        def fetchAll params
					admin = false
					if params.has_key? :all
						params.delete :all
						admin=true
					end

          where = self.model

          params.each do |key, param|
            if self.model.content_attributes.with_indifferent_access.has_key? key
              where = where.where("payload->>'#{key}' = ?", param)
              params.delete key
            end
          end

					params = self.community_filter(params)
          where.where(params)
        end

      end
    end
  end
end
