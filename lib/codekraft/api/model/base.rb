module Codekraft
  module Api
    module Model
      class Base < ActiveRecord::Base
        self.abstract_class = true

				after_save do |instance|
					if instance.has_attribute?(:serialized)
						serializerKlassName = "#{self.class.name.camelize}Serializer"
						if not (serializerKlassName.safe_constantize and serializerKlassName.safe_constantize.is_a?(Class))
              serializerKlassName = "#{self.class.name}"
							if serializerKlassName.include?("::Model")
								serializerKlassName["::Model"] = "::Serializer"
							end
							serializerKlassName = "#{serializerKlassName}Serializer"
						end

						if serializerKlassName.safe_constantize and serializerKlassName.safe_constantize.is_a?(Class)
							update_column(:serialized, serializerKlassName.constantize.new(self, {root: false}))
						end
					end
				end

      end
    end
  end
end
